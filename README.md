# CubaGenie

CubaGenie is a generator gem that sets up all the infrastructure you need in order to be immediately productive with the [Cuba](http://cuba.is/) web micro-framework. CubaGenie creates a skeleton Cuba app, configured to your preferences.

## Why use it?

[Cuba](http://cuba.is/) is a wonderful DSL that allows us to create simple web-apps quickly and easily. However, for more complex apps we would need to add extra functionality and components to the basic Cuba setup, things like view templates, data stores, static file serving, our favourite JS or CSS framework and so on. This can be a time-consuming task and it's not always obvious how to get everything working together. CubaGenie takes away the hassle by creating everything you need in one fell, quick swoop. 

## What does it create?
- A basic Cuba application ready to be ran (config, app-file and Gemfile)
- Ruby version management support files (.ruby-version and .ruby-gemset)
- Standard view templates (layout and home)
- A .git folder (does `git init` in your project)
- A Minitest setup and basic functional test template (optional)
- A Capybara setup and basic acceptance test template (optional)
- A Rakefile with basic test tasks (optional)
- A Twitter Bootstrap installation, hooked into the view templates (optional)


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cuba_genie'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cuba_genie

## Usage

In your shell, cd into your work directory and type:

```shell
cuba_genie new <your_app_name>
```

Then simply keep answering the questions CubaGenie asks you.

## Gem Development

After checking out the repo, run `rake test` to run the tests. You can also run `rake console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Embedded Development

CubaGenie is fully embeddable. This means you can incorporate its run-time in your own code and add your own generator commands to be ran in addition to the existing CubaGenie commands.  Embedding CubaGenie in your own code is very simple:

### 1. include CubaGenie
Add CubaGenie to your  *gemspec* file 

	spec.add_dependency "cuba_genie"

In your main gem file require CubaGenie


	require "my_gem/version"
	require 'cuba_genie'
	
	module MyGem
	
	end


### 2. Setup CLI class

Under your exe directory create a new file  *my_gem*. Make sure   *my_gem* has executable privileges, i.e. run `chmod 755 exe/my_gem`. The content of *my_gem* must be:


	require 'cuba_genie'
	
	CubaGenie::MyCLI.start(ARGV)

### 3. Add your command classes

In your source files, open up the CubaGenie module and add your own command classes. A command class must:

1. Derive from *Command* class
2. Define a *PRECEDENCE* constant which sets the order in which the class will be executed (if more than one command classes are defined)
3. Implement an initializer with a @*description* attribute which calls *super*.
4. Implement the #*execute* method, passing a block to *super*.

for instance,

	module CubaGenie
	  class MyClass < Command
	    PRECEDENCE = 1 #this means this class will be ran before any others you define
	
	    def initialize
	      @description = 'this is MyClass' #this will show when your class runs
	      super
	    end
	
	    def execute
	      super do
	        File.open('some_file', 'w') {|f| f.write("hello world!") }
		    # if you create files or dirs maje sure you add them to @files_created and @dirs_created
	        @files_created << "#{Dir.pwd}/'some_file'"
	        puts "********** executin class A"
	      end
	    end
	
	  end
	end
	
### 4. Build and run your gem
`bundle install && bundle exec rake install` will build and install your gem locally. `my_gem new my_project_name` will run all the  CubaGenie generators and will then run your own generator classes in the order specified by PRECEDENCE. Happy days!



## Roadmap

* Add RSpec support
* Add choice of templating engine, e.g. erb, haml, slim, etc.
* Add Redis support
* Add relational DB support (possibly through an ORM)
* Create generator commands for test, model and view templates
* Add support for user authentication 


## Contributing

1. Fork the repository
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a merge request (https://gitlab.com/<user-name>/cuba-genie/merge_requests)


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).


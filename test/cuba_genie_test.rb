require 'test_helper'
require 'fakefs/safe'

class CubaGenieTest < Minitest::Test

  DIR_MISSING_MSG = "'%s' directory doesn't exist"
  FILE_MISSING_MSG = "'%s' file doesn't exist"
  CORE_FILES_MSG = 'one or more of the core cuba files does not exist!'
  DIR_ROLLBACK_MSG = "Directory '%s' should have been rolled back"
  FILE_ROLLBACK_MSG = "FIle '%s' should have been rolled back"

  TEST_HELPER_PATH = 'test/test_helper.rb'
  HOME_VIEW_PATH = 'views/home.mote'
  LAYOUT_PATH = 'views/layout.mote'

  TEST_PROJECT_NAME = 'genie_test'


  def setup
    FakeFS.activate!
    FileUtils.mkdir 'temp'
    Dir.chdir('temp')
    @cmd_list = CommandList.new
  end

  def teardown
    FakeFS.deactivate!
    FakeFS::FileSystem.clear
  end

  def test_that_it_has_a_version_number
    refute_nil ::CubaGenie::VERSION
  end

  def test_it_does_basic_setup
    cmd = CubaGenie::CubaSetup.new project_name: TEST_PROJECT_NAME,
      minitest: false,
      capybara: false
    cmd.execute
    assert core_files_exist?, CORE_FILES_MSG
    assert_equal '1000', gem_lines_checksum
    assert File.readlines("Gemfile").grep(/gem 'pry-byebug'/).size > 0
  end


  def test_it_does_views_setup
    @cmd_list.add_command CubaGenie::CubaSetup.new project_name: TEST_PROJECT_NAME,
      minitest: true,
      capybara: false

    @cmd_list.add_command CubaGenie::ViewsSetup.new project_name: TEST_PROJECT_NAME
    @cmd_list.execute
    assert core_files_exist?, CORE_FILES_MSG
    assert (Dir.exist? "views"), DIR_MISSING_MSG % 'views'
    assert (File.exist? HOME_VIEW_PATH), FILE_MISSING_MSG % HOME_VIEW_PATH
    assert (File.exist? LAYOUT_PATH), FILE_MISSING_MSG % LAYOUT_PATH
  end

  def test_it_does_minitest_gems
    cmd = CubaGenie::CubaSetup.new project_name: TEST_PROJECT_NAME,
      minitest: true,
      capybara: false
    cmd.execute
    assert core_files_exist?, CORE_FILES_MSG
    assert_equal '1100', gem_lines_checksum
  end

  def test_it_does_minitest_capybara_gems
    cmd = CubaGenie::CubaSetup.new project_name: TEST_PROJECT_NAME,
      minitest: true,
      capybara: true
    cmd.execute
    assert core_files_exist?, CORE_FILES_MSG
    assert_equal '1111', gem_lines_checksum

  end


  def test_it_does_minitest_setup_no_capybara
    @cmd_list.add_command CubaGenie::CubaSetup.new project_name: TEST_PROJECT_NAME,
      minitest: true,
      capybara: false

    @cmd_list.add_command CubaGenie::ViewsSetup.new project_name: TEST_PROJECT_NAME
    @cmd_list.add_command CubaGenie::MinitestSetup.new project_name: TEST_PROJECT_NAME

    @cmd_list.execute
    assert core_files_exist?, CORE_FILES_MSG
    assert (Dir.exist? "test"), DIR_MISSING_MSG % 'test'
    assert (Dir.exist? "test/functional"), DIR_MISSING_MSG % 'test/functional'
    assert (File.exist? TEST_HELPER_PATH), FILE_MISSING_MSG % TEST_HELPER_PATH
    assert (File.exist? "test/functional/main_test.rb"), FILE_MISSING_MSG % "test/functional/main_test.rb"
    assert (File.exist? "Rakefile"), FILE_MISSING_MSG % "Rakefile"
    assert_equal '1100', gem_lines_checksum
    assert_equal 0, File.readlines(TEST_HELPER_PATH).grep(%r{require 'minitest/capybara'}).size
    assert_equal 0, File.readlines(TEST_HELPER_PATH).grep(%r{Capybara'}).size
    assert File.readlines(TEST_HELPER_PATH).grep(%r{Minitest::Reporters::DefaultReporter}).size > 0
  end

  def test_it_does_minitest_capybara_setup
    @cmd_list.add_command CubaGenie::CubaSetup.new project_name: TEST_PROJECT_NAME, 
    								minitest: true, 
    								capybara: true
    @cmd_list.add_command CubaGenie::ViewsSetup.new project_name: TEST_PROJECT_NAME
    @cmd_list.add_command CubaGenie::MinitestSetup.new project_name: TEST_PROJECT_NAME,  
    					reporter: 'RubyMine', 
    					capybara_setup: true
    @cmd_list.execute
    assert core_files_exist?, CORE_FILES_MSG
    assert (Dir.exist? "test"), DIR_MISSING_MSG % 'test'
    assert (Dir.exist? "test/functional"), DIR_MISSING_MSG % 'test/functional'
    assert (Dir.exist? "test/acceptance"), DIR_MISSING_MSG % 'test/acceptance'
    assert (File.exist? "test/functional/main_test.rb"), FILE_MISSING_MSG % "test/functional/main_test.rb"
    assert (File.exist? "Rakefile"), FILE_MISSING_MSG % "Rakefile"
	assert_equal '1111', gem_lines_checksum
    assert (File.exist? "test/acceptance/browser_test.rb"), FILE_MISSING_MSG % "test/acceptance/browser_test.rb"
    assert 0, File.readlines(TEST_HELPER_PATH).grep(%r{require 'minitest/capybara'}).size  > 0
    assert 0, File.readlines(TEST_HELPER_PATH).grep(%r{Capybara}).size  > 0
    assert File.readlines(TEST_HELPER_PATH).grep(%r{Minitest::Reporters::RubyMineReporter}).size > 0


  end


  def test_it_rolls_back_in_case_of_failure_1
    @cmd_list.add_command CubaGenie::CubaSetup.new project_name: TEST_PROJECT_NAME, 
    								minitest: true, 
    								capybara: true
    @cmd_list.add_command CubaGenie::ViewsSetup.new project_name: TEST_PROJECT_NAME
    test_cmd = CubaGenie::MinitestSetup.new project_name: TEST_PROJECT_NAME,  
    												reporter: 'Default', 
    												capybara_setup: true
    # force an error so that rollback can kick in
    test_cmd.test_helper = "not_there/test_helper.rb" 
    @cmd_list.add_command test_cmd
    @cmd_list.execute
    refute (Dir.exist? "test"), DIR_ROLLBACK_MSG % 'test'
    refute (Dir.exist? "views"),  DIR_ROLLBACK_MSG % 'views'
    refute (Dir.exist? "genie_test"),  DIR_ROLLBACK_MSG % TEST_PROJECT_NAME
    refute (Dir.exist? "public"),  DIR_ROLLBACK_MSG % 'public'
    refute core_files_exist?, "at least one of the main project files hasn't been rolled back"
  end


  def test_it_rolls_back_in_case_of_failure_2
    @cmd_list.add_command CubaGenie::CubaSetup.new project_name: TEST_PROJECT_NAME, 
    								minitest: true, 
    								capybara: true
    @cmd_list.add_command CubaGenie::ViewsSetup.new project_name: TEST_PROJECT_NAME, bootstrap_version: '3.3.5'
    test_cmd = CubaGenie::MinitestSetup.new project_name: TEST_PROJECT_NAME,  
    									reporter: 'Default', 
    									capybara_setup: true
    # force an error so that rollback can kick in
    test_cmd.test_helper = "not_there/test_helper.rb" 
    @cmd_list.add_command test_cmd
    @cmd_list.execute
    refute (Dir.exist? "test"), DIR_ROLLBACK_MSG % 'test'
    refute (Dir.exist? "views"),  DIR_ROLLBACK_MSG % 'views'
    refute (Dir.exist? "genie_test"),  DIR_ROLLBACK_MSG % TEST_PROJECT_NAME
    refute (Dir.exist? "public"),  DIR_ROLLBACK_MSG % 'public '

    refute core_files_exist?, "at least one of the main project files hasn't been rolled back"
  end  


  private

  def core_files_exist?
    File.exist?('genie_test.rb') &&
      File.exist?('config.ru') &&
      File.exist?('Gemfile')
  end

  def gem_lines_checksum
  	chksum = []
  	chksum[0] = File.readlines("Gemfile").grep(/gem 'minitest-reporters'/).size
  	chksum[1] = File.readlines("Gemfile").grep(/gem 'rack-test'/).size
    chksum[2] = File.readlines("Gemfile").grep(/gem 'minitest-capybara'/).size
    chksum[3] = File.readlines("Gemfile").grep(/gem 'selenium-webdriver'/).size 
	chksum.join
  end


end

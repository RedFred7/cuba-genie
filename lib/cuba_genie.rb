require "cuba_genie/version"
require "cuba_genie/content/content"
require "cuba_genie/content/content_helpers"

require 'thor'
require_relative 'cuba_genie/command_list'
require_relative 'cuba_genie/cuba_setup'
require_relative 'cuba_genie/minitest_setup'
require_relative 'cuba_genie/views_setup'

module CubaGenie

  POSITIVE_RESPONSES =  %w(y Y yes)
  NEGATIVE_RESPONSES =  %w(n N no)


  class MyCLI < Thor
    include Content

    desc "new PROJECT", "create project NAME"
    def new(project_name)
      @cmds = CommandList.new
      puts "your project name is '#{project_name}'"
      minitest = want_minitest?
      reporter = (minitest ? choose_reporter : 'n')
      capybara = (minitest ? want_capybara? : false)
      @cmds.add_command CubaSetup.new(project_name: project_name, minitest: minitest, capybara: capybara)
      @cmds.add_command ViewsSetup.new(project_name: project_name, bootstrap_version: choose_bootstrap_version)
      @cmds.add_command MinitestSetup.new(project_name: project_name, reporter: reporter, capybara_setup: capybara) if minitest

      # allow users to add their own commands
      run_extensions

      puts closing_message(project_name) if @cmds.execute
    end

    private
    def want_minitest?
      response = ask("Would you like to setup Minitest for your project?",
                     limited_to: POSITIVE_RESPONSES + NEGATIVE_RESPONSES)
      puts ""
      POSITIVE_RESPONSES.include? response
    end

    def want_capybara?
      response = ask("Would you like to create acceptance (Capybara) tests for your project?",
                     limited_to: POSITIVE_RESPONSES + NEGATIVE_RESPONSES)
      puts ""
      POSITIVE_RESPONSES.include? response

    end

    def choose_reporter
      puts "Choose a Minitest output format (reporter):"
      MINITEST_REPORTERS.each_with_index do |reporter, idx|
        puts "#{idx+1}: #{reporter.keys.first.to_s.extend(ContentHelpers).titlecase} (#{reporter.values.first})"
      end
      # response = ask_for_numeric_reponse (1..8)
      response = ask("Enter a number between 1 and 8:\n" )
      puts ""

      MINITEST_REPORTERS[response.to_i - 1].keys.first.to_s.extend(ContentHelpers).titlecase
    end

    def choose_bootstrap_version
      puts "Choose which version of Twitter Bootstrap you want to use:"
      (["Don't install Bootstrap"] + ALLOWED_BOOTSTRAP_VERSIONS).each_with_index do |opt, idx|
        puts "#{idx+1}: #{opt}"
      end
      # response = ask_for_numeric_reponse (1..5)
      response = ask("Enter a number between 1 and 5:\n" )
      puts ""

      (response.to_i == 1) ? 'n' : ALLOWED_BOOTSTRAP_VERSIONS[response.to_i - 2]
    end

    private

    # Gets all Command-derived classes defined in the CubaGenie
    # module and filters out the classes already defined by the
    # core CubaGenie gem. What is left are the classes added as
    # user-extensions to the gem, and which will be run after the
    # built-in commands have ran.
    #
    # @param N/A
    #
    # @return [Array<Symbol>] all user-added Command-derived classes
    #
    #
    def get_extensions
      existing_klasses = [:CubaSetup, :MinitestSetup, :ViewsSetup]
      all_command_klasses = CubaGenie.constants.select do |c|
        Class === CubaGenie.const_get(c) && CubaGenie.const_get(c).superclass == CubaGenie::Command
      end
      all_command_klasses - existing_klasses
    end

    def closing_message(project_name)
      "\nYour #{project_name} Cuba app is now set up and ready to go! To start the app, cd "\
        "into the #{project_name} directory, run 'bundle install' and then 'rackup'. You can then "\
        "see your app running on 'http://localhost:9292'."
    end


    # Gets all user-defined Command-derived classes within the
    # CubaGenie module, order them according to their PRECEDENCE
    # constant and adds them to the command-list so that they can
    # be executed after the built-in CubaGenie classes
    #
    # @param N/A
    #
    # @return [Array<Symbol>] all user-added Command-derived classes
    #
    #
    def run_extensions
      # first check if someone added extension classes in our module
      command_classes = get_extensions
      unless command_classes.empty?
        #order classes in order of precedence
        command_classes.sort_by! do |item|
          CubaGenie.const_get(item).const_get(:PRECEDENCE)
        end

        command_classes.each do |klass|
          if CubaGenie.const_get(klass).instance_methods.include? :execute
            @cmds.add_command CubaGenie.const_get(klass).new
          else
            # #execute method is not implemented
            ## TODO: raise exception?
          end
        end
      end
    end

  end #class
end #module

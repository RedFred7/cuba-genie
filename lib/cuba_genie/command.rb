module CubaGenie
# Implements the Command pattern. Responsible for providing execute
# and unexecute (rollback) methods. Every Command-derived class must
# implement an #execute methods which passes a block to super and
# (optionally) an unexecute method 

#
  class Command

    attr_reader :description, #what this class does
                :error, #fill-in if things go wrong
                :rollback_msg, #what to display if we roll-back
                :project_name


    def initialize(**args)
      # arrays used to keep track of files created, so that they
      # can be rolled back, if needed
      @files_created, @dirs_created = [], []
    end


    # Yields to a block where the user puts their command code 
    #
    # @param N/A
    #
    # @return [Boolean] true if methods runs without problems,
    # false if an exception is raised
    #
    # @note any files or directories created here should have their
    # path pushed to @files_created and @dirs_created arrays.
    #
    def execute
      yield
      true
    rescue Exception => e
      puts e.message and false
    end


    # Deletes all files and directories created with the execute
    # method.
    #
    # @param N/A
    #
    # @return [Boolean] true if methods runs without problems,
    # false if an exception is raised
    #
    #
    def unexecute
      puts @rollback_msg unless ENV['RACK_ENV'] == 'development'
      @files_created.each do |file_path|
        File.delete file_path if File.exist? file_path
      end
      @dirs_created.each do |dir_name|
        FileUtils.rm_rf(dir_name) if Dir.exist? dir_name
      end
    end

  end #class
end #CubaGenie

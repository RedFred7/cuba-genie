module Content


  APP_FILE_CONTENT =  %Q(
require "cuba"
require "mote"
require "mote/render"

Cuba.plugin(Mote::Render)

Cuba.use Rack::Static,
  root: "public",
  urls: ["/css","/js", "/fonts"]

Cuba.define do
  on root do
    res.write view("home", {title: "Cuba Genie"})
  end
end
  )


  RACK_FILE_CONTENT = %Q(
require "./%s"

run(Cuba)
  )



  TEST_HELPER_CONTENT_CORE = %Q(
ENV['RACK_ENV'] = 'development'
require 'minitest/autorun'
require 'rack/test'
require 'minitest/reporters'
Minitest::Reporters.use! [ Minitest::Reporters::%sReporter.new ]
%s

require_relative '../%s'


class RackTest < MiniTest::Test

  include Rack::Test::Methods

  def app
    Cuba
  end
end
  )

  TEST_HELPER_CONTENT_CAPYBARA = %Q(
Capybara.configure do |config|
  config.run_server = false
  config.default_driver = :selenium
  config.app_host = 'http://localhost:9292'
end

class AcceptanceTest < Minitest::Capybara::Test
end
  )


  MINITEST_REPORTERS = [
    { default: 'Redgreen-capable version of standard Minitest reporter'},
    { spec: 'Turn-like output that reads like a spec'},
    { progress: 'Fuubar-like output with a progress bar'},
    { rubyMate: 'Simple reporter designed for RubyMate'},
    { rubyMine: 'Reporter designed for RubyMine IDE and TeamCity CI server'},
    { jUnit: 'JUnit test reporter designed for JetBrains TeamCity'},
    { meanTime: 'Produces a report summary showing the slowest running tests'},
    { html: 'Generates an HTML report of the test results'}
  ]


  ACCEPTANCE_TEST_FILE_CONTENT = %Q(
require_relative '../test_helper.rb'

class AcceptanceTest
  def test_home
    visit "/"
    assert_content "Hello from Cuba Genie"
  end
end
  )


  FUNCTIONAL_TEST_FILE_CONTENT = %Q(
require_relative '../test_helper.rb'

class RackTest

  def test_hello_world
    get '/'
    assert last_response.ok?
    assert last_response.body.match(/Hello from Cuba Genie/)
  end

end
  )

  GEM_FILE_CONTENT = %Q(
source "https://rubygems.org"
gem 'cuba'
gem "mote"
gem "mote-render"

group :development, :test do
  gem 'pry-byebug'
  gem 'minitest-reporters'
  %s
  gem 'shotgun'
end
  )

  GEM_FILE_CAPYBARA_CONTENT = %Q(
gem 'rack-test'
gem 'minitest-capybara'
gem 'selenium-webdriver'
  )  


RAKE_FILE_CONTENT = %q(
require 'rake/testtask'

namespace :test do

  Rake::TestTask.new(:all) do |t|
    t.libs = %w(lib test)
    t.pattern = "test/**/*_test.rb"
  end


  %w(functional acceptance).each do |name|
    Rake::TestTask.new(name) do |t|
    t.libs = %W(lib/#{ name } test test/#{ name })
    t.pattern = "test/#{ name }/**/*_test.rb"
    end
  end

end

task test: ["test:all"]
task default: "test"

)  


BOOTSTRAP_CSS = %Q(
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/jumbotron.css" rel="stylesheet">
)

BOOTSTRAP_JS = %Q(
  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
)


end #module

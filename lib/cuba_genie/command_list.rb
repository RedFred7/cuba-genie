# Implements the Command pattern. Responsible for executing a command list and invoking rollback on any errors
#
class CommandList

  def initialize
    @commands = []
  end

  def add_command(cmd)
    @commands << cmd
  end

  def length
    @commands.size
  end

  def execute
    res = true
    @commands.each_with_index do |cmd, idx|
      unless cmd.execute
        puts "ERROR: #{cmd.error}"
        unexecute(idx)
        res = false
        break
      end
      puts cmd.description unless ENV['RACK_ENV'] == 'development'
    end
    res
  end


  def description
    description = ''
    @commands.each { |cmd| description += cmd.description + "\n" }
    description
  end

  private
  def unexecute(idx)
    @commands.slice(0, idx + 1).reverse_each do |cmd| 
      cmd.rollback_msg
      cmd.unexecute
    end
  end

  alias_method :size, :length

end #module

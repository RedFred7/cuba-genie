require 'fileutils'
require_relative 'content/content'
require_relative 'command'


module CubaGenie

  class MinitestSetup < Command
    include Content

    attr_writer :test_helper

    def initialize(**args)
      @description = (args[:capybara_setup] ?
                      "Setting up Minitest functional and acceptance tests" :
                      "Setting up Minitest functional tests")
      @rollback_msg = "rolling back Minitest"
      @capybara_setup = args[:capybara_setup]
      @project_name = args[:project_name]
      @reporter = args[:reporter] || 'Default'
      @test_helper = "test/test_helper.rb"
      super
    end

    def execute
      super do
        FileUtils.mkdir_p "test/functional"
        @dirs_created << "test" << "test/functional"
        if @capybara_setup
          FileUtils.mkdir_p "test/acceptance"
          @dirs_created << "test/acceptance"
        end
        create_test_helper
        create_rake_file
        create_functional_test
        create_acceptance_test if @capybara_setup
      end
    end


    private
    def create_test_helper

      gem_line_capybara = %q(require 'minitest/capybara')
      require_line = (@capybara_setup ? gem_line_capybara : nil )
      content = TEST_HELPER_CONTENT_CORE % [@reporter, require_line, @project_name]
      content = (content + TEST_HELPER_CONTENT_CAPYBARA) if @capybara_setup
      File.open(@test_helper, 'w') {|f| f.write(content) }
      @files_created << @test_helper

    end

    def create_rake_file
      File.open("Rakefile", 'w') {|f| f.write( RAKE_FILE_CONTENT) }
      @files_created << "#{Dir.pwd}/Rakefile"
    end

    def create_functional_test
      file_path = 'test/functional/main_test.rb'
      File.open(file_path, 'w') {|f| f.write(FUNCTIONAL_TEST_FILE_CONTENT) }
      @files_created << "#{Dir.pwd}/#{file_path}"

    end

    def create_acceptance_test
      file_path = 'test/acceptance/browser_test.rb'
      File.open(file_path, 'w') {|f| f.write(ACCEPTANCE_TEST_FILE_CONTENT) }
      @files_created << "#{Dir.pwd}/#{file_path}"
    end



  end #class

end #module

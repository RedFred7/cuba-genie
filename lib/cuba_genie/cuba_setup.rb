require 'fileutils'
require_relative 'content/content'
require_relative 'command'

module CubaGenie

  class CubaSetup < Command
    include Content

    def initialize(**args)
      @project_name = args[:project_name]
      @minitest = args[:minitest]
      @capybara = args[:capybara]
      @description = "Creating Cuba setup"
      @rollback_msg = "rolling back Cuba basic setup"
      super

    end

    def execute
      super do
        FileUtils.mkdir @project_name
        @dirs_created << @project_name
        FileUtils.chdir @project_name
        create_app_file
        create_rack_file
        create_gem_file
        git_initialize
        create_ruby_version_file
        create_ruby_gemset_file
      end
    end


    private

    def create_app_file
      file_name = "#{@project_name}.rb"
      File.open(file_name, 'w') {|f| f.write(APP_FILE_CONTENT) }
      @files_created << "#{Dir.pwd}/#{file_name}"
    end

    def create_rack_file
      File.open('config.ru', 'w') {|f| f.write(RACK_FILE_CONTENT % @project_name) }

      @files_created << "#{Dir.pwd}/config.ru"
    end

    def create_gem_file
      File.open('Gemfile', 'w') do |f|
        if @minitest && !@capybara
          f.write GEM_FILE_CONTENT % "gem 'rack-test'"
        elsif @minitest && @capybara
          f.write GEM_FILE_CONTENT % GEM_FILE_CAPYBARA_CONTENT
        elsif !@minitest
          f.write GEM_FILE_CONTENT
        end
      end
      @files_created << "#{Dir.pwd}/Gemfile"
    end

    def create_ruby_version_file
      File.open('.ruby-version', 'w') {|f| f.write(get_ruby_version) }
      @files_created << "#{Dir.pwd}/.ruby-version"
    end

    def create_ruby_gemset_file
      File.open('.ruby-gemset', 'w') {|f| f.write(project_name) }
      @files_created << "#{Dir.pwd}/.ruby-gemset"
    end

    def get_ruby_version
      %x(ruby -v).slice(/(\d\.){2}\d/)
    end

    def git_initialize
      %x(git init)
    end    

  end #class
end #module

# Change Log


## 0.1.0 (2016-04-10)

-- Initial release

## 0.1.1 (2016-04-11)

-- Fixed bug by 'require pry' line left in code

## 0.1.2 (2016-04-11)

-- Some serious code refactoring

## 0.1.3 (2016-04-16)

-- Don't have to implement #unexecute in command classes any longer, just set @rollback_msg 
-- added git support
-- enabled gem as embeddable, i.e. other gems can now incorporate their own functionality into CubaGenie code.

# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cuba_genie/version'

Gem::Specification.new do |spec|
  spec.name          = "cuba_genie"
  spec.version       = CubaGenie::VERSION
  spec.authors       = ["RedFred"]
  spec.email         = ["fred_h@bootstrap.me.uk"]

  spec.summary       = %q{CubaGenie is a generator gem that sets up all the infrastructure you need in order to be immediately productive with the Cuba web development DSL }
  spec.description   = %q{CubaGenie creates a skeleton Cuba app, configured to your preferences, which may include view templates, test templates, CSS framework and many more.}
  spec.homepage      = "https://gitlab.com/RedFred7/cuba-genie"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.8"
  spec.add_development_dependency 'minitest-reporters', '~> 1.0'
  spec.add_development_dependency "fakefs"
  spec.add_development_dependency "m"
  spec.add_development_dependency "pry-byebug"  
  spec.add_development_dependency "pry-stack_explorer"  
  spec.add_development_dependency "simplecov"  
  spec.add_dependency "thor"
  spec.add_dependency "rubyzip"
end
